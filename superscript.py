import websocket
import os
from gpiozero import Button
from gpiozero import LED
from threading import Thread
import RPi.GPIO as GPIO
from time import sleep
from threading import Timer
import time
import math
import statistics
import threading
import encoder_volume as encoder_volume
from neopixel import *
import argparse
import signal
import VL53L0X


# Create a VL53L0X object
tof = VL53L0X.VL53L0X()

# Start ranging
tof.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
# LED strip configuration:
LED_COUNT      = 9     # Number of LED pixels.
LED_PIN        = 12      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

GPIO.setmode(GPIO.BCM)

# Assigning pins to buttons
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(27, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(17, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down = GPIO.PUD_UP)
GPIO.setup(4, GPIO.IN, pull_up_down = GPIO.PUD_UP)
              
# Global variables
ledOn = False
proximityresults = []
global buttonData
inProximity = False
radioState = False # this variable tells us if we are playing from radio audio

encoderON = False
lastEncoderTime = time.time()
isMute = False

isCalling = False


# set up the SPI interface pins
#GPIO.setup([SPIMOSI, SPICLK, SPICS], GPIO.OUT)
#GPIO.setup(SPIMISO, GPIO.IN)

# defining and initializing strip
strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)    # Intialize the library (must be called once before other functions).
strip.begin()



# Setup websocket
def on_message(ws, message):
    
    print("On message: " + message)
    
    messageArray = message.split('-')
    
    if messageArray[0] == "idle":
        idleChanged(messageArray[1])
    elif messageArray[0] == "radioStation":
        radioStationChanged(messageArray[1])

def on_close(ws):
    print("### closed ###")

ip = "169.254.0.20" #IP of where the node server is
wsPort = "40510"
ws = websocket.WebSocketApp("ws://" + ip + ":" + wsPort + "/websocket", on_message = on_message, on_close = on_close)
#websocket.enableTrace(True)
wst = threading.Thread(target=ws.run_forever)
wst.daemon = True
wst.start()

# data to save
buttonData = {
    "button": "0",   
}

knobValueData = {
    "knobValue": "0",
}

 
   
# threaded function that reads proximity values every 1 second(s)
def proximity_check():
    global proximityresults
    global inProximity
    threading.Timer(1.0, proximity_check).start()
    for x in range(10):
        proximityVal = tof.get_distance()
        if proximityVal > 1200:
            proximityVal = 0
        else:
            proximityVal = 1
        proximityresults.append(proximityVal)
    average = statistics.mean(proximityresults)
    #stderror = statistics.stdev(proximityresults)/10.0
    proximityresults = []
    sleep(0.02)

    if average >= .3 and not inProximity:
        inProximity = True
        #print("In Proximity")
        ws.send("proximityState-true")
    elif average  < .3:
        if inProximity == True:
            ws.send("proximityState-false")
        inProximity = False
        #print("Out of Proximity")



# Encoder variables- GPIO Button is the mute button
MUTE_BUTTON = encoder_volume.MUTE_BUTTON
Enc_A = encoder_volume.GPIO_A
Enc_B = encoder_volume.GPIO_B
# function that gets triggered when mute button is pressed
def on_press(vol):
    # print("MUTE")
    # print(radioState)
    # if radioState:
    # global isMute
    # isMute = not isMute
    # ledVolumeControl
    # v.toggle()
    # encoder_volume.EVENT.set()
    # thread = Thread(target= timerTurnOff)
    # thread.start()
    # ledVolumeControl(strip)
    button7Pressed()

def on_exit(pins, MUTE_BUTTON):
    print("Exiting...")
    encoder_volume.sys.exit(0)
  
def on_exit(a, b):
    print("Exiting...")
    encoder.destroy()
    encoder_volume.sys.exit(0)
  
def on_turn(delta):
    if not isMute:
        encoder_volume.QUEUE.put(delta)
        encoder_volume.EVENT.set()
        
# makes encoder and volume instance from encoder_volume file
if MUTE_BUTTON != None:
    encoder = encoder_volume.RotaryEncoder(Enc_A, Enc_B, callback=on_turn, buttonPin=MUTE_BUTTON, buttonCallback=on_press, saveFile="file.txt")
    v = encoder_volume.Volume()
    signal.signal(signal.SIGINT, on_exit)
  
# Process rotary encoder values
def consume_encoder_queue():
    global encoderON
    global lastEncoderTime
    while not encoder_volume.QUEUE.empty():
      delta = encoder_volume.QUEUE.get()
      handle_delta(delta)
      encoderON = True
      lastEncoderTime = time.time()

def handle_delta(delta):
    if v.is_muted:
      v.toggle()
    if delta == 1:
      vol = v.up()
    else:
      vol = v.down()
    ledVolumeControl(strip)

# Conerts RGB values between 0-255 into a 8 bit number
def Color(red, green, blue, white = 0):
    return (white << 24) | (red << 16)| (green << 8) | blue

# Sets led strip to RGB color it is given
def setPixelColorRGB(strip, red, green, blue, white = 0):
    """Set LED at position n to the provided red, green, and blue color.
    Each color component should be a value from 0 to 255 (where 0 is the
    lowest intensity and 255 is the highest intensity).
    """ 
    for i in range(strip.numPixels()):
        
        strip.setPixelColor(i, Color(red, green, blue, white))
    strip.show()
		
# Sets LED strip to a rainbow animation		
def rainbow(strip, wait_ms=20, iterations=1):
    global isCalling
    isCalling = True
    """Draw rainbow that fades across all pixels at once."""
    brightness = [0, 2, 4, 8, 16, 32, 64, 96, 128, 160, 192, 224, 240, 255]
    start_time = time.time()
    brightnessSteps = len(brightness)
    fullBrightSteps = 5
    initialColor = 120
    
    # fade in
    for j in range(brightnessSteps):
        for i in range(strip.numPixels()):
                
              #if time.time() - start_time < 5.0:
                strip.setPixelColor(i, wheel((i+j) & 255, brightness, j, "in"))
                #strip.show()
              #else:
               #return
                #time.sleep(0.05)
        strip.show()
        time.sleep(0.08)
    #full brightness
    for j in range(fullBrightSteps):
          #if time.time() - start_time < 5.0:
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j + brightnessSteps) & 255, brightness, j, "normal"))
            #strip.show()
        strip.show()
        time.sleep(0.08)
    
    for j in range(brightnessSteps):
        for i in range(strip.numPixels()):
              #if time.time() - start_time < 5.0:
                strip.setPixelColor(i, wheel((i+j + brightnessSteps + fullBrightSteps) & 255, brightness,j,"out"))
                
                #time.sleep(0.05)
        strip.show()
        time.sleep(0.1)
    isCalling = False
 
# Helper function for LED color method
def wheel(pos, brightness, j, state):
    shifted = pos + 120
    """Generate rainbow colors across 0-255 positions."""
    if shifted < 85:
        
        if state == "in":
            return Color(int(shifted*3 * brightness[j]/255.0), int((255 - shifted * 3) *brightness[j]/255.0), 0) #pos*3
        elif state == "out":
            return Color(int(shifted*3 * brightness[13 - j]/255.0), int((255 - shifted * 3)* brightness[13 -j]/255.0), 0)
        else:
            return Color(shifted*3 , (255 - shifted * 3), 0)
         
        
    elif shifted < 170:
        shifted -= 85
        
        if state == "in":

            return Color(int((255 - shifted*3)* brightness[j]/255.0), 0, int(shifted * 3* brightness[j]/255.0))
        elif state == "out":
            return Color(int((255 - shifted*3)* brightness[13 - j]/255.0), 0, int(shifted * 3 * brightness[13 -j]/255.0))
        else:
            return Color(255 - shifted*3, 0, shifted * 3)
    else:
        shifted -= 170
        
        if state == "in":
            return Color(0, int(shifted*3* brightness[j]/255.0), int((255 - shifted * 3)*brightness[j]/255.0))
        elif state == "out":
            return Color(0, int(shifted*3* brightness[13 -j]/255.0), int((255 - shifted * 3)* brightness[13 -j]/255.0))
        else :
            return Color(0, shifted*3, 255 - shifted * 3)
     
    
# function that controls LEDS based on volume value   
def ledVolumeControl(strip, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    color = Color(230, 230, 230)
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0,0,0))
    time.sleep(0.05)
    
    num = int(encoder_volume.GLOBAL_VOLUME/10.0)
  
    if num != 0:    
        for i in range(num):
            strip.setPixelColor(8 - i, color)
            
    strip.show()
    
   
# LED animation when social call happens
def fadeInfadeOut():
    global isCalling
    isCalling = True
    brightness = [2,4,8,16,32,64,128,192, 224, 255, 224, 192, 128, 64, 32, 16, 8, 4, 2, 0, 0, 0, 0, 0, 0]
    pixelBrightness = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    while ledOn:
        for i in brightness:
            for j in range(4):
                pixelBrightness[8 - j] = pixelBrightness[8 - j- 1]
                pixelBrightness[j] = pixelBrightness[1 + j]
            pixelBrightness[4] = i
            
            for pixel in range(5):
                r1 = int(20*(pixelBrightness[4 - pixel]/255.0))
                g1 = int(180*(pixelBrightness[4 - pixel]/255.0))
                b1 = int(200*(pixelBrightness[4 - pixel]/255.0))
                
                r2 = int(20*(pixelBrightness[4 + pixel]/255.0))
                g2 = int(180*(pixelBrightness[4 + pixel]/255.0))
                b2 = int(200*(pixelBrightness[4 + pixel]/255.0))
                if ledOn:
                    strip.setPixelColor(4-pixel, Color(g1,r1,b1))
                    strip.setPixelColor(4+pixel, Color(g2,r2,b2))
                #strip.show()
                #time.sleep(0.05)
            strip.show()
            time.sleep(0.1)
    isCalling = False    
         
# turns off LED    
def turnOff():
    time.sleep(0.1)
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0,0,0))
    strip.show()

# asynchronous button press function 
def button1Pressed(channel):
    print("Button1 pressed")
    global buttonData
    buttonData["button"] = "1"
    ws.send("button-" + buttonData["button"])
    
# asynchronous button press function 
def button2Pressed(channel):
    print("Button2 pressed")
    global buttonData
    buttonData["button"] = "2"
    ws.send("button-" + buttonData["button"])
   
# asynchronous button press function
def button3Pressed(channel):
    print("Button3 pressed")
    global buttonData
    buttonData["button"] = "3"
    ws.send("button-" + buttonData["button"])

# action to take when button 4 is pressed
def button4Pressed(channel):
    print("Button4 pressed")
    global buttonData
    buttonData["button"] = "4"
    ws.send("button-" + buttonData["button"])
   
# action to take when button 5 is pressed   
def button5Pressed(channel):
    print("Button5 pressed")
    global buttonData
    buttonData["button"] = "5"
    ws.send("button-" + buttonData["button"])
    
# action to take when button 6 is pressed    
def button6Pressed(channel):
    print("Button6 pressed")
    global buttonData
    buttonData["button"] = "6"
    ws.send("button-" + buttonData["button"])

# action to take when button 7 is pressed    
def button7Pressed():
    print("Button7 pressed")
    global buttonData
    buttonData["button"] = "7"
    ws.send("button-" + buttonData["button"])
    
# assigning button pins to asynchronous functions
GPIO.add_event_detect(24, GPIO.FALLING, callback = button2Pressed, bouncetime=300)
GPIO.add_event_detect(23, GPIO.FALLING, callback = button1Pressed, bouncetime=300)
GPIO.add_event_detect(4, GPIO.FALLING, callback = button3Pressed, bouncetime=300)
GPIO.add_event_detect(22, GPIO.FALLING, callback = button6Pressed, bouncetime=300)
GPIO.add_event_detect(27, GPIO.FALLING, callback = button5Pressed, bouncetime=300)
GPIO.add_event_detect(17, GPIO.FALLING, callback = button4Pressed, bouncetime=300)

# Websocket listeners
def radioStationChanged(radioNumber):
    
    global radioState
    global ledOn

    if int(radioNumber) > 8:
       #os.system("mpc volume 100")
       radioState = False
    
    if radioNumber == "0":
        os.system("mpc stop")   
        radioState = False
    else:
        os.system("mpc play " + radioNumber)
        if int(radioNumber) < 9 or int(radioNumber) > 12:
            radioState = True
        
    if radioNumber == "12" or radioNumber == "11": 
        ledOn = True
        thread = Thread(target= fadeInfadeOut)
        thread.start()
        
    if radioNumber == "10" or radioNumber == "9":    
        ledOn = False
        time.sleep(0.05)
        turnOff()

# idle state handle - If we are transitiong from idle to non idle then run animation
def idleChanged(state):
    if state == "false":
        rainbow(strip)
        turnOff()

# initializes radio - need to add all audio files at the start from folder - order of adding audiofiles matters
def initialize_radio():
    turnOff()
    os.system("mpc update")
    os.system("mpc clear")
    os.system("mpc add EuropaFM.mp3")
    os.system("mpc add CadenaDial.mp3")
    os.system("mpc add Radiole.mp3")
    os.system("mpc add Cope.mp3")
    os.system("mpc add OndaCero.mp3")
    os.system("mpc add Cadena100.mp3")
    os.system("mpc add KissFM.mp3")
    os.system("mpc add CadenaSer.mp3")
    os.system("mpc add phoneConversation.mp3")
    os.system("mpc add socialTalkManAndWomanEdited.mp3")
    os.system("mpc add incomingCall.m4a")
    os.system("mpc add outgoingcall.m4a")
    os.system("mpc add teleHealthAudio.mp3")
    os.system("mpc add videoAudio.mp3")
    os.system("mpc add BBC.mp3")
    os.system("mpc add CapitalFM.mp3")
    os.system("mpc add LBC.mp3")
    os.system("mpc add MagicRadio.mp3")
    os.system("mpc add TalkRadio.mp3")
    os.system("mpc add RadioX.mp3")
    os.system("mpc add Siren.mp3")
    os.system("mpc add SOSAudio.mp3")
    os.system("mpc repeat on")
    os.system("mpc single on")
   
proximity_check()
initialize_radio()

    
if __name__ == "__main__":
    
    
    while True:
        if not isMute and not isCalling:
            # print("Is calling: " + str(isCalling))
            #encoder_volume.EVENT.wait(1200)
            global lastEncoderTime
            global encoderON
            consume_encoder_queue()
            if time.time() - lastEncoderTime >  2.0 and encoderON:
                encoderON = False
                turnOff()
            encoder_volume.EVENT.clear()